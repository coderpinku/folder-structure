// Traverse node custom hook 
const useTraverseNode = () => {

  // Inserting Node function 

  function insertNode(node, folderId, item, isFolder) {
    if (node.id === folderId && node.isFolder) {
      node.items.unshift({
        id: new Date().getTime(),
        name: item,
        isFolder,
        items: []
      });

      return node;
    }
    let latestNode = [];
    latestNode = node.items.map((ob) => {
      return insertNode(ob, folderId, item, isFolder)
    });
    return { ...node, items: latestNode }
  }

  // Deleting Node function 

  const deleteNode = (node, folderId) => {
    if (!node.items) return node

    let removedNode = []
    removedNode = node.items
      .map((item) => {
        return deleteNode(item, folderId)
      })
      .filter(item => item.id !== folderId);
    return { ...node, items: removedNode }
  }

  // Editing node Function 
  
  const editNode = (node, folderId, newName) => {

    if (!node.items) {
      return node;
    }
    if (node.id === folderId) {
      return { ...node, name: newName };
    }
    let latestName = node.items.map((item) => {
      return editNode(item, folderId, newName)
    })
    return { ...node, items: latestName };
  }
  return { insertNode, deleteNode, editNode }
};

export default useTraverseNode;