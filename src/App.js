import { useState } from "react";
import Folder from "./components/Folder";
import explorer from "./data/folderData";
import './style.css'
import useTraverseNode from "./custom-hooks/traverseNode"
function App() {

  const [explorerData, setExplorerData] = useState(explorer);

  // Calling custom hooks 
  const { insertNode, deleteNode, editNode } = useTraverseNode();

  // handling insert node 
  const handleInsertNode = (folderId, item, isFolder) => {
    const finalNode = insertNode(explorerData, folderId, item, isFolder)
    setExplorerData(finalNode);
  }
//  handling delete node 
  const handleDeleteNode = (folderId) => {
    const deletedNode = deleteNode(explorerData, folderId)
    setExplorerData(deletedNode)
  }

  // handling edit node 
  const handleUpdateNode = (folderId, newName) => {
    const updatedNode = editNode(explorerData, folderId, newName)
    setExplorerData(updatedNode)
  }
  return (
    <div className="app--js">
      <Folder
        handleInsertNode={handleInsertNode}
        handleDeleteNode={handleDeleteNode}
        handleUpdateNode={handleUpdateNode}
        explorer={explorerData}
      />
    </div>
  );
}

export default App;
