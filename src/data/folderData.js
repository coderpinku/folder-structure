// Folder data 

const explorer = {
  id: "1",
  name: "EVALUATION",
  isFolder: true,
  items: [
    {
      id: "2",
      name: "Applications",
      isFolder: true,
      items: [

        {
          id: "3",
          name: "pycharm.dmg",
          isFolder: false,
          items: []
        },
        {
          id: "4",
          name: "Webstrom.dmg",
          isFolder: false,
          items: []
        },
        {
          id: "5",
          name: "fileZilla.dmg",
          isFolder: false,
          items: []
        },


        {
          id: "6",
          name: "Mattermost.dmg",
          isFolder: false,
          items: []
        }
      ]
    },

    {
      id: "7",
      name: "Desktop",
      isFolder: true,
      items: [
        {
          id: "8",
          name: "Screenshot1.jpg",
          isFolder: false,
          items: []
        },
        {
          id: "9",
          name: "videopal.mp4",
          isFolder: false,
          items: []
        },
        {
          id: "10",
          name: "playstore_screenshot.png",
          isFolder: false,
          items: []
        }
      ]
    },
    {
      id: "11",
      name: "Technical design",
      isFolder: true,
      items: [
        {
          id: "12",
          name: "productDesign.js",
          isFolder: false,
          items: []
        },
        {
          id: "13",
          name: "system.dll",
          isFolder: false,
          items: []
        }
      ]
    },
    {
      id: "14",
      name: "Documents",
      isFolder: true,
      items: [
        {
          id: "15",
          name: "Document1.jpg",
          isFolder: false,
          items: []
        },
        {
          id: "16",
          name: "Document2.jpg",
          isFolder: false,
          items: []
        },
        {
          id: "17",
          name: "Document3.txt",
          isFolder: false,
          items: []
        }
      ]
    },
    {
      id: "18",
      name: "Downloads",
      isFolder: true,
      items: [
        {
          id: "19",
          name: "Drivers",
          isFolder: true,
          items: [
            {
              id: "20",
              name: "cameradriver.dmg",
              isFolder: false,
              items: []
            },
            {
              id: "21",
              name: "Printerdriver.dmg",
              isFolder: false,
              items: []
            }
          ]
        }
      ]
    },
    {
      id: "22",
      name: "chromedriver.dmg",
      isFolder: false,
      items: []
    }

  ]
}
export default explorer;  