import { useState } from "react";
import { VscChevronRight, VscChevronDown } from "react-icons/vsc";
import { AiOutlineFileAdd, AiOutlineFolderAdd, AiOutlineDelete, AiFillEdit } from "react-icons/ai"

function Folder({ handleInsertNode, explorer, handleDeleteNode, handleUpdateNode }) {

    // folder structure data 
    const [data, setData] = useState(explorer);

    // Expanding folder or file state 
    const [expand, setExpand] = useState(false);

    // Editing data state
    const [editData, setEditData] = useState(-1);

    // Displaying Input 
    const [showInput, setShowInput] = useState({
        visible: false,
        isFolder: null
    });

    // Checking folder  or file function 

    const handleNewfolder = (e, isFolder) => {
        e.stopPropagation();
        setExpand(true);
        setShowInput({
            visible: true,
            isFolder
        });
    }

// Add folder function 

    const onAddFolder = (e) => {
        if (e.keyCode === 13 && e.target.value) {
            handleInsertNode(explorer.id, e.target.value, showInput.isFolder)
            setShowInput({ ...showInput, visible: false })
        }
    }

//    Deleting Node items 

    const onDeleteNode = (e, key,) => {
        e.stopPropagation();
        handleDeleteNode(key)
        setData(item => [item, ...data])

    }

//  Handle Editing folder or file node

    const handleEditNode = (e, itemId) => {
        e.stopPropagation();
        setEditData(itemId)
    }

    const onEdit = (e) => {
        if (e.keyCode === 13) {
            handleUpdateNode(explorer.id, e.target.value)
            setEditData({ ...editData }, (!editData))
        }
    }



    if (explorer.isFolder) {

        return <div className="App" style={{ marginLeft: 15 }}>
            {
                editData === explorer.id ?

                    <div className="input--container">

                        <input
                            className="input--box"
                            autoFocus
                            name="name"
                            Value={explorer.name}
                            onKeyDown={onEdit}
                            onBlur={() => setEditData(!editData)}
                            type="text"
                        />
                    </div>

                    :
                    <div className="folder" onClick={() => setExpand(!expand)}>

                        <div className="folder--container">

                            <span>
                                {expand ? (<VscChevronDown size={25} />) : (<VscChevronRight size={25} />)}
                            </span>
                            <span>
                                📁{explorer.name}
                            </span>

                        </div>

                        <div className="Icons">
                            <AiFillEdit onClick={(e) => handleEditNode(e, explorer.id)} className="crud--icons" />
                            <AiOutlineDelete onClick={(e) => onDeleteNode(e, explorer.id)} className="crud--icons" />
                            <AiOutlineFileAdd onClick={(e) => handleNewfolder(e, false)} className="crud--icons" />
                            <AiOutlineFolderAdd onClick={(e) => handleNewfolder(e, true)} className="crud--icons" />
                        </div>

                    </div>
            }

            <div style={{ display: expand ? "block" : "none", paddingLeft: 20 }}>
                {
                    showInput.visible && (
                        <div className="input--container">
                            <span>{showInput.isFolder ? "📁" : "📄"}</span>
                            <input
                                className="input--box"
                                onKeyDown={onAddFolder}
                                autoFocus
                                onBlur={() => setShowInput({ ...showInput, visible: false })}
                                type="text"
                            />
                        </div>
                    )
                }

                <div>
                </div>
                
                {explorer.items.map((exp) => {
                    return <Folder
                        handleInsertNode={handleInsertNode}
                        handleDeleteNode={handleDeleteNode}
                        handleUpdateNode={handleUpdateNode}
                        explorer={exp}
                        key={exp.id}
                    />
                })}

            </div>
        </div>
    } else {
        return <div className="files">
            {

                editData === explorer.id ?

                    <div className="input--container">

                        <input
                            className="input--box"
                            autoFocus
                            name="name"
                            Value={explorer.name}
                            onKeyDown={onEdit}
                            onBlur={() => setEditData(!editData)}
                            type="text"
                        />
                    </div>
                    :
                    <div className="editFiles">

                        <span className="file">📄{explorer.name}</span>
                        <span className="file files--icon">
                            <AiFillEdit onClick={(e) => handleEditNode(e, explorer.id)} className="crud--icons" />
                            <AiOutlineDelete onClick={(e) => onDeleteNode(e, explorer.id)} className="crud--icons" />
                        </span>
                    </div>
            }
        </div>
    }
}
export default Folder;